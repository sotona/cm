#ifndef G_H
#define G_H

const int nd = 5;


typedef double v1 [ 5/*# range 1..nd*/ ];
typedef int v2 [ 5/*# range 1..nd*/ ];
typedef double masc [ 5/*# range 1..nd*/ ][ 5/*# range 1..nd*/ ];

void plsu(double **a, const double* b, double *x, int n );

#endif // G_H
