#ifndef UNIDIMENSIONAL_OPT_H
#define UNIDIMENSIONAL_OPT_H
#include "cm.h"
#include <float.h>
namespace cm{
//unidimensional optimization
namespace unidimensional_opt{
//---------------------------------------------------------------half-dvision-for-many-variable-function's
//optimize the function f by variable number var_n ;a,b - left and right border's of optimization
//m = (-1, 1) depends of optimization task (min, max); 
//return optimal value of variable number var_n
double half_division(vector<double> &_start_point, double a, double b,
                                unsigned var_n, double (*f)(vector<double>),double accuracy=cm::def_accuracy,int m=1);
/// gold section optimization
double gold_section(double a,double b, foo f, void*p,const double_vector &point,
													 const double_vector &dir, double acc);
}}


#endif
