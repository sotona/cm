#include "cm.h"

namespace cm{
using namespace std;
const double def_accuracy = 0.001;

namespace DXDY{
//---------------------------------------------------------dfdxi
double dfdxi(const foo f, void*param, double_vector &x, unsigned i, double dx){
x.at(i)+=dx;
double fx=f(x,param);
x.at(i)-=dx;
return (fx-f(x,param))/dx;}
//---------------------------------------------------------gradient
double_vector gradient(double_vector &vec, double dx, const foo f, void*param){
double_vector grad;
for (unsigned i=0; i<vec.size(); i++){
grad.push_back(dfdxi(f,param,vec,i,dx));}
return grad;}
//---------------------------------------------------------частная производная
//вторая производная функции f по переменной x[i]
double d2fdxi2(const foo f, void*p,double_vector x, unsigned i,double dx){
double fx, fx_,_fx;
fx=f(x, p);
x.at(i)+=dx; fx_=f(x,p);
x.at(i)-=2*dx; _fx=f(x,p);
return (_fx-2*fx+fx_)/dx/dx;}
//---------------------------------------------------------Hesse matrix
double **getHesseMatrix(const foo f, void*p, double_vector x, double dx){
double **hesse;
double_vector x_(x);
hesse = new double*[2];
hesse[0] = new double [2];
hesse[1] = new double [2];
hesse[0][0] = d2fdxi2(f,p,x,0);
hesse[1][1] = d2fdxi2(f,p,x,1);
x_.at(1)+=dx;
hesse[1][0] = (d2fdxi2(f,p,x_,0) - d2fdxi2(f,p,x,0))/dx;
x_.at(1)=x.at(1); x_.at(0)+=dx;
hesse[0][1] = (d2fdxi2(f,p,x_,1) - d2fdxi2(f,p,x,1))/dx;

return hesse;}
}
// System of Linear Equation
namespace SoLE{
// чёртовы шаблоны...
}
}

