#include "unidimensional_opt.h"
#include "multidimensional_opt.h"
namespace cm{
namespace multidimensional_opt{
const unsigned long max_steps = 0xffff;
//-------------------------------------------------------------constructor (accuracy)
Gradient_descent::Gradient_descent(double _accuracy):multidim_opt(_accuracy){}
//-------------------------------------------------------------constructor (function, accuracy)
Gradient_descent::Gradient_descent(foo f, double accuracy):multidim_opt(f,accuracy){}
//--------------------------------------------------------------lamda
double Gradient_descent::getOptLambda(double_vector &point,double_vector &dir){
return cm::unidimensional_opt::gold_section(-accuracy,-1000.0,this->func,param,point, dir, accuracy);}
//--------------------------------------------------------------find min
double_vector Gradient_descent::getOptimalValues(std::vector<double> &_start_position){
double_vector _vec(_start_position.size()), vec_(_start_position), grad(_start_position.size(),0);
double _lambda=0;
if (log) {this->vector_steps.push_back(_start_position);
		  this->lambda_steps.push_back(_lambda);
		  this->gradients.push_back(grad);}
iterations=0;
do{
copy(vec_.begin(), vec_.end(), _vec.begin());

//Vx_ = _Vx + lambda * Vgrad

grad = cm::DXDY::gradient(_vec, accuracy*2, this->func, param);
_lambda=this->getOptLambda(vec_,grad);
for (unsigned i=0; i<_vec.size(); i++)
vec_.at(i)=_vec.at(i) + _lambda*grad.at(i);
if (log) {this->gradients.push_back(grad);
		  this->lambda_steps.push_back(_lambda);
		  this->vector_steps.push_back(vec_);}
// если сделали очень много шагов, но так и не получили приемлемого решения
iterations++;
        if (this->max_iterations && vector_steps.size()>max_iterations) break;
}while(fabs(this->func(_vec, param) - this->func(vec_, param))>accuracy);
return vec_;}

        //--------------
        #define period(i) unsigned(pow(3.0,(double)i))
        #define plus_minus_nothing(i,j) j%(period(i+2))/period(i+1)

/// оптимизация функции
double_vector Hooke_Jeeves::getOptimalValues(double_vector  &_start_position){
        double_vector x_(_start_position), _x(_start_position.size());
        double lam;
        double_vector dir(_start_position.size(),0);
        clearReports();

         if (log) this->vector_steps.push_back(x_);
         this->lambdas.push_back(0.0);
         this->directions.push_back(dir);
iterations=0;
        do{
        copy(x_.begin(), x_.end(), _x.begin());
        double_vector dir(_start_position.size(),0);
        double new_step_len=step_len;
        //поиск вокруг базисной точки
        while (!search_around(dir, x_) && new_step_len>=accuracy) new_step_len/=10;
        this->directions.push_back(dir);

        if (new_step_len<accuracy) {
        //this->lambdas.push_back(0.0);
        break;}
        lam=search_in_direction(_x, dir);
        for (unsigned i=0; i<_x.size(); i++)
                x_[i]=_x[i]+dir[i]*lam;
        this->lambdas.push_back(lam);
        if (log) this->vector_steps.push_back(x_);
		iterations++;
        // если сделали очень много шагов, но так и не получили приемлемого решения
        if (this->max_iterations && iterations >max_iterations) break;
        }while(fabs(func(x_,param)-func(_x,param))>accuracy);

        return x_;}

        //----------------------- поиск вогруг базисной точки
        bool Hooke_Jeeves::search_around(double_vector &direction, double_vector &point){
        double_vector testing_point(point);
        unsigned in_all_points=(unsigned)pow(3.0,(double)point.size());	//количество рассматриваемых точек
        double opt_func=func(point, param), new_opt=func(point, param);
        unsigned opn=0; //optimal point number
        double_vector *dir;/* = new double_vector(direction.size());*/

        //поиск оптимальной точки
        for (unsigned tpn=0; tpn<in_all_points; tpn++){ //testing point number - tpn
         dir = getDirection(tpn);
         for (unsigned i=0; i<direction.size(); i++)
                testing_point[i]=point[i]+dir->at(i);
         new_opt=func(testing_point, param);
         if (opt_func>new_opt) {opt_func=new_opt; opn=tpn;}
         delete dir;}

         if (opt_func==func(point,param))	return 0;
         else dir = getDirection(opn);
         copy(dir->begin(), dir->end(), direction.begin());
         return 1;
         }
        //----------------------- поиск по образцу
        double Hooke_Jeeves::search_in_direction(double_vector &point,double_vector &dir){
		return cm::unidimensional_opt::gold_section(accuracy,1000.0,this->func,param, point, dir, accuracy);

		}
        //------------------------ testing points
        const std::list<double_vector>* Hooke_Jeeves::TestingPoints(){
        std::list<double_vector> *tp = new std::list<double_vector>();
        unsigned n=TestingPointsAtStep();

        double_vector tmp(vector_steps.front().size());
        for (std::list<double_vector>::iterator it=vector_steps.begin(), end=vector_steps.end(); it!=end; it++)
        for (unsigned j=0; j<n; j++){
        double_vector *dir = getDirection(j);
         for (unsigned i=0; i<vector_steps.front().size(); i++)
         tmp[i]=(*it).at(i)+dir->at(i);
         tp->push_back(tmp);}
        return tp;}
        //------------------------ point namber index about basis
        double_vector* Hooke_Jeeves::getDirection(unsigned index){
        double_vector *d = new double_vector(this->vector_steps.front().size(),0);
        switch(index%3){
         case 0: {d->at(0)=step_len;	break;}
         case 2: {d->at(0)=-step_len;	break;}}
        for (unsigned i=0; i<d->size()-1; i++)
        switch(plus_minus_nothing(i,index)){
         case 0: {d->at(i+1)=step_len;	break;}
         case 2: {d->at(i+1)=-step_len;	break;}}
        return d;
        }
        //------------------------ clear reports
        void Hooke_Jeeves::clearReports(){
        this->vector_steps.clear();
        this->lambdas.clear();
        this->directions.clear();}


//----------------------------------------------------------get optimal values

double_vector Newton::getOptimalValues(double_vector  &_start_position){
_start_position.resize(2);  //хм... 2?
double_vector x_(_start_position), _x(_start_position.size()), grad(_start_position.size(),0);
clearReports();
this->Hesse_matrix = cm::DXDY::getHesseMatrix(func,param,_start_position);
makeInvHesseMatrix();
if (log) this->gradients.push_back(grad);
this->iterations=0;
do{
if (log) this->vector_steps.push_back(x_);
copy(x_.begin(), x_.end(), _x.begin());
grad = cm::DXDY::gradient(_x,accuracy*2,this->func, param);
if (log) this->gradients.push_back(grad);
for (unsigned i=0; i<_x.size(); i++)
	x_.at(i)=_x.at(i) - (invHesse_Matrix[0][i]*grad.at(0) + invHesse_Matrix[1][i]*grad.at(1));
this->iterations++;
if (this->max_iterations && this->iterations>max_iterations) break; // если сделали очень много шагов, но так и не получили приемлемого решения
}while(fabs(func(x_,param)-func(_x,param)) > accuracy ||
        (dfdxi(func,param,x_,accuracy,0)+dfdxi(func,param,x_,accuracy,1))>accuracy);
if (log) this->vector_steps.push_back(x_);
return x_;}

//-------------------------------- make invert Hesse matrix
void Newton::makeInvHesseMatrix(){
invHesse_Matrix=new double*[2];
for(int i=0;i<2;i++)
invHesse_Matrix[i]=new double[2];
 double s=Hesse_matrix[0][0]*Hesse_matrix[1][1] -
Hesse_matrix[1][0]*Hesse_matrix[0][1]; //Hessenian
invHesse_Matrix[0][0]=Hesse_matrix[1][1]/s;
invHesse_Matrix[0][1]=(-1.)*Hesse_matrix[0][1]/s;
invHesse_Matrix[1][0]=(-1.)*Hesse_matrix[1][0]/s;
invHesse_Matrix[1][1]=Hesse_matrix[0][0]/s;}

void Newton::clearReports(){
this->gradients.clear();
this->vector_steps.clear();}
}}
