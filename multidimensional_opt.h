#ifndef MULTIDIMENSIONAL_OPT_H
#define MULTIDIMENSIONAL_OPT_H
#include "cm.h"

namespace cm{
namespace multidimensional_opt{
extern const unsigned long max_steps;   ///< maximum amount of algorithm steps
using namespace DXDY;

/// базовый класс для методов многомерной оптимизации
class multidim_opt{
protected:
         foo func;                                          ///< оптимизируемая функция
         void* param;                                       ///< param for func
		 double accuracy;                                   ///< точность (если шаг функции оптимизируемой меньше чем точность, то стоп)
         unsigned long max_iterations;                      ///< максимальное число шагов цикла вычислений. 0, если не ограничено
		 unsigned long iterations;
		 bool log;											///< save vector at each optimization step?
         std::list<double_vector> vector_steps;             ///< vectors at each step
         void clearReports(){this->vector_steps.clear();}   ///< очистка отчета (списка векторов на каждом шаге и т.п.)
		 bool is_enough_iteraions() const {return (max_iterations && iterations>max_iterations);}
public:		
		multidim_opt(double accuracy=cm::def_accuracy)
			{this->accuracy=accuracy;
			 max_iterations=max_steps;
			 iterations=0;
			 log=0;}
		multidim_opt(foo f, double accuracy=cm::def_accuracy)
			{this->func=f;
			this->accuracy=accuracy;
			max_iterations=max_steps;
			iterations=0;
			log=0;}
		void Accuracy(double a){this->accuracy=a;}
		double Accuracy(){return this->accuracy;}
	
        const std::list<double_vector>& Steps(){return this->vector_steps;}
        void setFunction(foo f, void* param=NULL){this->func=f; this->param=param;}
	
		unsigned iterations_count(){return iterations;}
        long itarations_maximum() const {return max_iterations;}
        void setIterations_maximum(unsigned long n) {max_iterations=n;}
    virtual double_vector getOptimalValues(double_vector  &_start_position){return this->vector_steps.back();}
};


class Gradient_descent:public multidim_opt{
private:
 std::list<double_vector> gradients;   //gradient at each step
 std::list<double>		  lambda_steps;		//lambda at each step
 //-----------------
double getOptLambda(double_vector &point,double_vector &dir);
public:
 Gradient_descent(double _accuracy=cm::def_accuracy); 
 Gradient_descent(foo f, double accuracy=cm::def_accuracy);
//---------------
 double_vector getOptimalValues(double_vector  &_start_posiion);
//---------------
const std::list<double_vector>& Gradients(){return this->gradients;}
const std::list<double>& Lambdas(){return this->lambda_steps;}
};



/// метод Хука-Дживса
class Hooke_Jeeves:public multidim_opt{
private:
	double step_len; //длинна шаги при поиске по образцу 
	bool search_around(double_vector &direction, double_vector &point); //поиск по образцу. изменяет direction - направление относительно текущей точки.
								//возвращает 1 когда поиск удался. в качестве насальной точки использует vector_steps.back()
								
	double search_in_direction(double_vector &point,double_vector &dir);//поиск оптимума в заданом направлении. возвращает длинну шага
	double_vector* getDirection(unsigned index);
	//-------------
	std::list<double_vector> directions;
	std::list<double> lambdas;
public:
	Hooke_Jeeves(double step_len,double accuracy=cm::def_accuracy):multidim_opt(accuracy),step_len(step_len){}
        Hooke_Jeeves(foo f, double accuracy=cm::def_accuracy):multidim_opt(f,accuracy){}
	double_vector getOptimalValues(double_vector  &_start_posiion);
	
	void StepLen(double step_len_){this->step_len = step_len_;}
	double StepLen(){return this->step_len;}
	
	//отчет
	const std::list<double>* Lambdas() {return &(this->lambdas);}
	const std::list<double_vector>* Directions() {return &(this->directions);}
	const std::list<double_vector>* TestingPoints(); //список точек, рассматривавшихся при поиске вокруг базисной точки
	
	unsigned TestingPointsAtStep(){
	unsigned n=this->vector_steps.front().size();
	n=(unsigned)pow(3.0, (int)n);
	return n;}
	
	void clearReports();
};		 

/// метод Ньютона для 2-х переменных
class Newton:public multidim_opt{
private:
	double **Hesse_matrix;	//[2][2]
	double **invHesse_Matrix;  //Inverse matrix [2][2] 
        std::list<double_vector> gradients;	  //gradient vector's at each steps
	//-----------
	void makeInvHesseMatrix();
	//-----------
	void clearReports();
public:
	//std::list<double_vector> gradients;	  //gradient vector's at ech steps
	Newton(double accuracy=cm::def_accuracy):multidim_opt(accuracy){}
	Newton(foo f, double accuracy=cm::def_accuracy):multidim_opt(f,accuracy){}
	
	double_vector getOptimalValues(double_vector  &_start_position);
	
	double** getHesseMetrix() const{return this->Hesse_matrix;}
	double** getInvHesseMatrix() const {return this->invHesse_Matrix;}
	
	const std::list<double_vector>* Gradients() {return &(this->gradients);}
};
}}

#endif // MULTIDIMENSIONAL_OPT_H
