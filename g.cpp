#include <math.h>
#include "g.h"


void psu_2( double** a, const double* b, double* x, int n, unsigned* im, int& ier )
{
  int i = 0, j = 0, n1 = 0, i1 = 0, k = 0, k1 = 0;
  double s = 0.0;

  for (i = 0; i <n; i++)
  x[i] = b[im[i]];

  n1 = n - 1;

  for (i = 0; i <n1; i++){
    i1 = i + 1;
    for (int k = i1; k <n; k++)
      x[k] = x[k] - a[k][i] * x[i];
  }

  x[n-1] = x[n-1]/a[n-1][ n-1];

  for (k = n1-1; k >=0; k--)
  {
    s = x[k];
    k1 = k + 1;
    for (j = k1; j <n; j++)
      s = s - a[k][j] * x[j];
    x[k] = s / a[k][k];
  }
}
//-------------------------------------------------------------------------------



void lsu_2( double** a, const double* b, double* x, int n, unsigned* im, int& ier )
{
  int i = 0, j = 0, n1 = 0, m = 0, k = 0, s = 0, i1 = 0/*, ie = 0*/;
  double am = 0.0, c = 0.0;
  ier = 1;
  for (i = 0; i <n; i++)
    im[i] = i;
  n1 = n - 1;
  for (i = 0; i <n1; i++)
  {
    m = i;
    am = pow( a[i][i],2 );
    for (k = i; k <n; k++)
    {
      if ( pow( a[k][i],2) - am > 0 )
      {
        am = pow( a[k][i],2);
        m = k;
      }
    }
    if ( m != i )
    {
      s = im[i];
      im[i] = im[m];
      im[m] = s;
      for (j = 0; j <n; j++)
      {
        c = a[i][j];
        a[i][j] = a[m][j];
        a[m][j] = c;
      }
    }
    i1 = i + 1;
    if ( a[i][i] == 0 )
    {
      ier = 0;
      return;
    }
    for (k = i1; k <n; k++)
    {
      if ( a[k][i] != 0 )
      {
        c = a[k][i] / a[i][ i];
        a[k][i] = c;
        for (j = i1; j <n; j++)
          a[k][j] = a[k][j] - c * a[i][j];
      }
    }
  }
  if ( a[n - 1][ n - 1] == 0 )
  {
    ier = 0;
  }
}


void plsu(double **a, const double* b, double *x, int n ){
  int ier = 0;
  unsigned im[n];
  lsu_2( a, b, x, n, im, ier );
  psu_2( a, b, x, n, im, ier );
}
