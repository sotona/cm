#include "unidimensional_opt.h"
namespace cm{
//unidimensional optimization
namespace unidimensional_opt{
/// half-dvision-for-many-variable-function's
/// optimize the function f by variable number var_n ;a,b - left and right border's of optimization
/// m = (-1, 1) depends of optimization task (min, max);
/// @return optimal value of variable number var_n
double half_division(vector<double> &_start_point, double a, double b,
                                unsigned var_n, double (*f)(vector<double>),double accuracy,int m){
double _a=a;
vector<double> start_point(_start_point);
double ff;
do
{_a=a;
start_point.at(var_n)=a+(b-a)/4.0;
double f1=m*f(start_point);start_point.at(var_n)=(a+b)/2.0;
ff=m*f(start_point);start_point[var_n]=b-(b-a)/4.0;
double f2=m*f(start_point);
short c=min(f1,ff); c+=min(ff,f2);

switch(c){
case 0:{a=(a+b)/2.0; break;}
case 2:{b=(a+b)/2.0; break;}
case 1:{a=a+(b-a)/4.0; b=b-(b-_a)/4.0; break;}}
}while (fabs(b-a)>=accuracy);
return start_point[var_n]=(a+b)/2.0;}



/// gold section optimization
/// @param a,b - range of optimazation
/// @param point - vector, parameters of finction
/// @param dir - indicates optimizing param. exmpl: {0,0,1} for optimizing variable #3
/// @param acc - accuracy
double gold_section(double a,double b, foo f, void*p,const double_vector &point,
						const double_vector &dir, double acc){
double_vector new_point(point.size());

double t=(-1.+sqrt(5.0))/2.;
double x1=b-(b-a)*t;
double x2=a+t*(b-a);

for (unsigned i=0; i<point.size(); i++)
 new_point[i]=(fabs(dir[i])<DBL_EPSILON)?point[i]:x1; // эту ли переменную мы оптимизируем?
double y1=f(new_point,p);

for (unsigned i=0; i<point.size(); i++)
 new_point[i]=(fabs(dir[i])<DBL_EPSILON)?point[i]:x2; // эту ли переменную мы оптимизируем?
double y2=f(new_point,p);

do{
if (y2<y1){
a=x1;
x1=x2;
y1=y2;
x2=a+t*(b-a);

for (unsigned i=0; i<point.size(); i++) new_point[i]=(fabs(dir[i])<DBL_EPSILON)?point[i]:x2;

y2=f(new_point,p);}
else{
b=x2;
x2=x1;
y2=y1;
x1=b-(b-a)*t;

for (unsigned i=0; i<point.size(); i++) new_point[i]=(fabs(dir[i])<DBL_EPSILON)?point[i]:x1;

y1=f(new_point,p);}}
while( fabs(b-a)>acc);
 return (a+b)/2;
}
}}
