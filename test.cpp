#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include "multidimensional_opt.h"
#include "unidimensional_opt.h"

using namespace std;
using namespace cm;
using namespace cm::multidimensional_opt;
using namespace cm::unidimensional_opt;

double e = 0.000001;

namespace func{
namespace miltiD{
double himmelblau(double_vector& vec,void*){
double x=vec[0], y=vec[1];
return pow(x*x+y - 11.0, 2.) + pow(x+y*y-7.0, 2.);}

double rosenbrock(double_vector& vec,void*){
double x=vec[0], y=vec[1];
return pow(1.-x*x, 2.0) + 100*pow(y-x*x, 2.0);}}

namespace oneD{
double sqrx(double_vector& vec,void*){
return vec[0]*vec[0];}

double _J(double_vector& vec,void*){
if (vec[0]>0) return pow(vec[0],10.); else
return -vec[0]/100.;}
}
}

namespace RosenbrockMin{
const double x=1.0;
const double y=1.0;
const double f=0;}

namespace HimmelblauMin{
const double x1=3.0;
const double y1=2.0;
const double x2=-2.805118;
const double y2=3.131312;
const double x3=-3.779310;
const double y3=-3.283186;
const double x4=3.584428;
const double y4=-1.848126;
const double min=0;}

void printHemmelblauMin(ofstream &f){
using namespace HimmelblauMin;
f << "Hemmelblau function's minimums:"<<endl << "("<<x1<<", "<<HimmelblauMin::y1<<") "<< "("<<x2<<", "<<y2<<") "
<< "("<<x3<<", "<<y3<<") "<< "("<<x4<<", "<<y4<<") " <<endl;
f << "F = "<<HimmelblauMin::min<<endl;}

void print_result(ofstream &file, double_vector v, foo f){
file << "f("<<v[0]<<", "<<v[1]<<") = "<<f(v,NULL)<<endl;
file << "function error " <<f(v,NULL)-HimmelblauMin::min<<endl;
}



int main(int argc, char**argv){
int X=7;
X=X/2;
ofstream f("test_result");
f << "test of unidimensional methods"<<endl;
f << "gold section: "<< endl;
double a=-120, b = 190;
f << "f = x*x, a =" << a<<", b = "<<b<<endl;
double_vector vec(1), vecd(1);
vecd[0]=1;
double x = gold_section(a,b,func::oneD::sqrx,NULL,vec, vecd,e);
vec[0]=x; f << "f("<<x<<") = "<< func::oneD::sqrx(vec,NULL)<<endl;
f << "f = (x>0)? x^10 : -x/100"<< endl;
x = gold_section(a,b,func::oneD::_J,NULL,vec, vecd,e);
vec[0]=x; f << "f("<<x<<") = "<< func::oneD::_J(vec,NULL)<<endl;

//f << "test of miltidimensional methods"<<endl;
//printHemmelblauMin(f);
//double_vector start(2);
//start[0]=-0.270845;start[1]=-0.923029;	// local maximum for himmelbla function
//f << "Himmelblau function"<< endl;
//f << "start from: ("<<start[0]<<", "<<start[1]<<")"<<endl;

//cm::multidimensional_opt::Gradient_descent GD(himmelblau,e);
//cm::multidimensional_opt::Hooke_Jeeves HJ(himmelblau,e);
//cm::multidimensional_opt::Newton N(himmelblau,e);
//double_vector result=GD.getOptimalValues(start);
//f << "Gradient Descent:"<<endl; print_result(f,result,himmelblau);
//result=HJ.getOptimalValues(start);
//f << "Hooke Jeeves:"<<endl; print_result(f,result,himmelblau);
//result=N.getOptimalValues(start);
//f << "Newton:"<<endl; print_result(f,result,himmelblau);

//f<< "----------------------"<<endl;
//start[0]=0.;start[1]=-5.;
//f << "Rosenbrock's function. f_min(1,1) = 0"<<endl;
//GD.setFunction(rosenbrock); result=GD.getOptimalValues(start);
//f << "Gradient Descent:"<<endl; print_result(f,result,himmelblau);
//HJ.setFunction(rosenbrock); result=HJ.getOptimalValues(start);
//f << "Hooke Jeeves:"<<endl; print_result(f,result,himmelblau);
//N.setFunction(rosenbrock); result=N.getOptimalValues(start);
//f << "Newton:"<<endl; print_result(f,result,himmelblau);
f.close();


return 1;}
