#ifndef CM_H
#define CM_H

#define _USE_MATH_DEFINES

#include <list>
#include <vector>
#include <algorithm>
#include <math.h>
namespace cm{
#define DBL_EPSILON 2.2204460492503131e-16
using namespace std;
typedef std::vector<double> double_vector ;
typedef double(*foo)(double_vector& vec,void*);
extern const double def_accuracy;

namespace DXDY{
//---------------------------------------------------------dfdxi
double dfdxi(const foo f, void*param, double_vector &x, unsigned i, double dx=cm::def_accuracy);
//---------------------------------------------------------gradient
double_vector gradient(double_vector &vec, double dx, const foo f, void*param);
//---------------------------------------------------------частная производная
//вторая производная функции f по переменной x[i]
double d2fdxi2(const foo f, void*p, double_vector x, unsigned i,double dx=cm::def_accuracy);
//---------------------------------------------------------Hesse matrix
double **getHesseMatrix(const foo f, void*param, double_vector x, double dx=cm::def_accuracy);
}

/* System of Linear Equation */
namespace SoLE{
 //меняем местами строки в массиве с коэфицентами уравнения. mas[n][n+1]
template <typename T>
void row_swap(T **mas, int n,int from, int to){
     for(int i=0; i < n+1; i++){
         mas[from][i] = mas[from][i] + mas[to][i];
         mas[to][i] = mas[from][i] - mas[to][i];
         mas[from][i] = mas[from][i] - mas[to][i];}}

// определяем ближайшую строку с не нулевым элементом на диагонали
template <typename T>
int get_n_no_empty(T **a, int n, int begin){
     for(int i=begin; i < n; i++)
         if(a[i][i] != 0) return i;
     else return -1;}

// a - матрица коэффициентов + столбец свободных членов
// n - число строк
template <typename T>
bool Gaussian_elimination(T** a, T*x, unsigned n){
for (int i=0; i<n-1; i++){ // прямой проход
// вдруг на главной диагонали ноль?
int r=get_n_no_empty(a,n,i);
if (r==-1) return 0;
else if (r!=i) row_swap<T>(a,n,i,r);

for (int j=i; j<n-1; j++){ // сложение строк
float x=a[j+1][i]/a[i][i];
for (int l=0; l<n+1 && x!=0; l++){
a[j+1][l]-=a[i][l]*x;}}}

T s; // обратный проход
for (int i=n-1; i>=0; i--){
s=a[i][n];
for (int j=i+1; j<n; j++) s-=x[j]*a[i][j];
x[i]=s/a[i][i];}}
}}

#endif
